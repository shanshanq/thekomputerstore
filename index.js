const balanceElement = document.getElementById("balance");
const outstandingLoanElement = document.getElementById("outstanding-loan");
const payElement = document.getElementById("pay");

const computersElement = document.getElementById("computers");
const featureElement = document.getElementById("features");
const nameElement = document.getElementById("name");
const priceElement = document.getElementById("price");
const imageElement = document.getElementById("image");
const descriptionElement = document.getElementById("description");

const getLoanButton = document.getElementById("get-loan-button");
const bankButton = document.getElementById("bank-button");
const workButton = document.getElementById("work-button");
const repayButton = document.getElementById("repay-button");
const buyButton = document.getElementById("buy-button");

let computers = [];
let balance = 1000; // default balance : 1000
let outstandingloan = 0;
let pay = 0;
let hidden = true;
let price = 200;

balanceElement.innerText = "Balance: " + balance + " NOK";
payElement.innerText = "Pay: " + pay + " NOK";
if(hidden){
  repayButton.style.visibility = "hidden";
}


fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
  .then((response) => response.json())
  .then((data) => (computers = data))
  .then((computers) => addComputersToMenu(computers));

const addComputersToMenu = (computers) => {
  computers.forEach((element) => {
    addComputerToMenu(element);
  });
};

const addComputerToMenu = (computer) => {
  const computerElement = document.createElement("option");
  computerElement.value = computer.id;
  computerElement.appendChild(document.createTextNode(computer.title));
  computersElement.appendChild(computerElement);
  //display the first laptop
  priceElement.innerText = computers[0].price + " NOK";
  nameElement.innerText = computers[0].title;
  descriptionElement.innerText = computers[0].description;
  imageElement.src =
    "https://noroff-komputer-store-api.herokuapp.com/" + computers[0].image;
  featureElement.innerText = computers[0].specs;
};

const handleComputerMenuChange = (e) => {
  const selectedComputer = computers[e.target.selectedIndex];
  priceElement.innerText = selectedComputer.price + " NOK";
  price = selectedComputer.price;
  nameElement.innerText = selectedComputer.title;
  descriptionElement.innerText = selectedComputer.description;
  imageElement.src =
    "https://noroff-komputer-store-api.herokuapp.com/" + selectedComputer.image;
  featureElement.innerText = selectedComputer.specs;
};

const handleGetLoan = () =>{
  if(outstandingloan === 0){
    outstandingloan = prompt("Please enter the amount of money you want to get loan from the bank: ");
  }else{
    alert("You must pay the loan back first!!!");
  }
  
  if(outstandingloan <= balance*2 && outstandingloan > 0){
    outstandingLoanElement.innerText = "Outstanding Loan: " + outstandingloan + " NOK";
    balance = parseFloat(balance) + parseFloat(outstandingloan);
    balanceElement.innerText = "Balance: " + balance + " NOK";
    repayButton.style.visibility = "visible";
  }else if(outstandingloan > balance*2){
    alert("You can't get a loan more than double of your bank balance!!!")
  }else{
    alert("Please enter a valid number!!!")
  } 
}

const handleBank = () =>{
  if(pay === 0){
    alert("No money to transfer:(");
  }
  if(outstandingloan === 0){
    balance += pay;
    pay = 0;
  }else{
    outstandingloan = parseFloat(outstandingloan) + parseFloat(pay * 0.1);
    balance = parseFloat(balance) + parseFloat(pay * 0.9);
    pay = 0;
  }
  balanceElement.innerText = "Balance: " + balance + " NOK";
  payElement.innerText = "Pay: " + pay + " NOK";
  if(outstandingloan !== 0){
    outstandingLoanElement.innerText = "Outstanding Loan: " + outstandingloan + " NOK";
  }
  
}

const handleWork = () =>{
  pay += 100;
  payElement.innerText = "Pay: " + pay + " NOK";
}

const handleRepay = () =>{
  if(pay < outstandingloan){
    outstandingloan -= pay;
    pay = 0;
  }
  if(pay >= outstandingloan){
    pay -= outstandingloan;
    outstandingloan = 0;
  }

  if(outstandingloan >= 0){
    outstandingLoanElement.innerText = "Outstanding Loan: " + outstandingloan + " NOK";
    payElement.innerText = "Pay: " + pay + " NOK";
  }else{
    repayButton.style.visibility = "hidden";
    outstandingLoanElement.style.visibility = "hidden";
  }
}

const handleBuy = () =>{
  if(balance < price){
    alert("Sorry, you don't have enough money in the bank!");
  }else{
    balance -= price;
    balanceElement.innerText = "Balance: " + balance + " NOK";
    alert("Congrats! You are the owner of the new laptop!");
  }

}

computersElement.addEventListener("change", handleComputerMenuChange);
getLoanButton.addEventListener("click", handleGetLoan);
workButton.addEventListener("click", handleWork);
bankButton.addEventListener("click", handleBank);
repayButton.addEventListener("click", handleRepay);
buyButton.addEventListener("click", handleBuy);